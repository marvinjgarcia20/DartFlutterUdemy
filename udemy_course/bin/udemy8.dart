void main() {
  final myName = 'Vin';

  // myName = 'test'; //Would give error as variable is final
  print(myName);
}

// Section 11: Final Keyword