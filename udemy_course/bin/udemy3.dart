void main() {
  var person1 = Person('Vin');
  var person2 = Person('Len');

  person1.printName();
  person2.printName();
}

class Person {
  String firstName;

  Person(this.firstName);

  void printName() {
    print(firstName);
  }
}

//Section 2