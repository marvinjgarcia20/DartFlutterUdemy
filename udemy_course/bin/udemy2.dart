void main() {
  var name = 'Vin';
  var num = 123;
  var num2 = 123.456;
  print('String: $name');
  print('Integer: $num');
  print('Double: $num2');

  dynamic x = 'String';
  print('\nDynamic: $x');

  x = 100;
  print('Dynamic: $x');

  x = 100.02;
  print('Dynamic: $x');
}

// Section 2