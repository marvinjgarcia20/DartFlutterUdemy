// import 'dart:html';
// import 'dart:async';

// void main() {
//   final InputElement input = querySelector('input');
//   final DivElement div = querySelector('div');

//   final emailValidator =
//       StreamTransformer.fromHandlers(handleData: (inputValue, sink) {
//     if (inputValue.contains('@')) {
//       sink.add(inputValue);
//     } else {
//       sink.addError('Enter a valid email');
//     }
//   });

//   input.onInput
//       .map((dynamic event) => event.target.value)
//       .transform(emailValidator)
//       .listen((inputValue) => div.innerHtml = '',
//           onError: (err) => div.innerHtml = err);
// }

// //Section 13: Streams - Validation

// /*
// <h4>Enter your email</h4>
// <input/>
// <div style="color: red"></div>
// */