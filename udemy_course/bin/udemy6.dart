import 'dart:convert';

void main() {
  var rawJson = '{"url": "http//blah.jpg", "id": 1}';

  var parsedJson = json.decode(rawJson);
  var imageModel = ImageModel.fromJson(parsedJson);
  print(imageModel);
}

class ImageModel {
  late int id;
  late String url;

  ImageModel(this.id, this.url);

  ImageModel.fromJson(parsedJson) {
    id = parsedJson['id'];
    url = parsedJson['url'];
  }

  @override
  String toString() {
    return 'ID: $id \nURL: $url';
  }
}

//Section 11: JSON