void main() {
  var circleSlot = Slot<Circle>();
  circleSlot.insert(Circle());
}

class Circle {}

class Square {}

class Slot<T> {
  void insert(T shape) {}
}

// Section: 10 - Generics: Object<Generic>()