void main() {
  PowerGrid grid = new PowerGrid();
  NuclearPlant nuclear = new NuclearPlant();
  SolarPlant solar = new SolarPlant();

  grid.addPlant(nuclear);
  grid.addPlant(solar);
  nuclear.meltdown();
}

class PowerGrid {
  List<PowerPlant> connectedPlants = [];

  addPlant(PowerPlant plant) {
    bool confirmation = plant.turnOn('5 hours');
    connectedPlants.add(plant);
  }
}

abstract class PowerPlant {
  int? energyCost;

  bool turnOn(String duration);
}

abstract class ABuilding {
  double? height;
}

class NuclearPlant implements PowerPlant {
  int? energyCost;

  @override
  bool turnOn(String d) {
    print('I\'m a nuclear plant turning on for $d.');
    return true;
  }

  meltdown() {
    print('F');
  }
}

class SolarPlant implements PowerPlant, ABuilding {
  int? energyCost;
  double? height;

  @override
  bool turnOn(String d) {
    print('I\'m a solar plant turning on for $d.');
    return true;
  }
}
