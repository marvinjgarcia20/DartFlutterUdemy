import 'dart:async';

class Cake {
  var type;

  Cake(this.type);

  @override
  String toString() {
    return '$type cake';
  }
}

class Order {
  String type;

  Order(this.type);
}

void main() {
  final controller = StreamController();

  final order = Order('Strawberry');

  final baker = StreamTransformer.fromHandlers(
    handleData: (cakeType, sink) {
      if (cakeType == 'Chocolate') {
        sink.add(Cake(cakeType));
        sink.add(Cake(cakeType));
      } else {
        sink.addError('I can\'t bake that type.');
      }
    },
  );

  controller.sink.add(order);

  controller.stream.map((order) => order.type).transform(baker).listen(
        (cake) => print('Here\'s your $cake'),
        onError: (err) => print(err),
      );
}

//Section 12 - Streams Intro