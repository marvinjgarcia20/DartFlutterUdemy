//Creating a Deck of Cards program

void main() {
  var deck = Deck();

  // deck.shuffle();
  deck.removeCard('Diamonds', 'Four');
  print('Deck: $deck');
  // print('Deck: ${deck.cardsWithSuit('Hearts')}');
  // print('Hand: ${deck.deal(5)}');
  // print('Left: $deck');
}

class Deck {
  List<Card> cards = [];

  Deck() {
    var ranks = ['Ace', 'Two', 'Three', 'Four', 'Five'];
    var suits = ['Diamonds', 'Hearts', 'Clubs', 'Spades'];

    for (var suit in suits) {
      for (var rank in ranks) {
        var card = Card(suit: suit, rank: rank);
        cards.add(card);
      }
    }
  }

  @override
  String toString() {
    return cards.toString();
  }

  void shuffle() {
    cards.shuffle();
  }

  // cardsWithSuit(String suit) {
  //   return cards.where((card) {
  //     return card.suit == suit;
  //   });
  // }

  // Refactor
  Iterable cardsWithSuit(String suit) {
    return cards.where((card) => card.suit == suit);
  }

  List<Card> deal(int handSize) {
    var hand = cards.sublist(0, handSize);
    cards = cards.sublist(handSize);

    return hand;
  }

  void removeCard(String suit, String rank) {
    cards.removeWhere((card) => (card.suit == suit) && (card.rank == rank));
  }
}

class Card {
  String suit;
  String rank;

  // Card({this.rank, this.suit});
  Card({required this.rank, required this.suit});

  @override
  String toString() {
    return '$rank of $suit';
  }
}

// Section 3