import 'dart:async';

void main() async {
  print('About to fetch data...');
  var result = await get('http:/abscdefg');

  print(result);

  // get('http:/testing123').then((result) {
  //   print(result);
  // });
}

Future<String> get(String url) {
  return Future.delayed(Duration(seconds: 3), () {
    return 'Got the data!';
  });
}

//Section 11: Future Class
