import 'dart:async';

void main() {
  final bloc = Bloc();

  bloc.email.listen((value) {
    print(value);
  });

  bloc.changeEmail('My New Email');
  bloc.changePassword('mypassword');
}

class Bloc {
  final emailController = StreamController<String>();
  final passwordController = StreamController<String>();

  Function(String) get changeEmail => emailController.sink.add;
  Function(String) get changePassword => passwordController.sink.add;

  Stream<String> get email => emailController.stream;
  Stream<String> get password => emailController.stream;
}
