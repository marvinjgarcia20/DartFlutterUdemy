//import 'dart:io'; //from the Dart standard library
import 'package:flutter/material.dart'; //from an external library
import 'src/app.dart'; //from an internal library

void main() {
  runApp(App());
}
